var game = new Phaser.Game(800, 500, Phaser.AUTO, '');

var alienData;
//https://gamedevacademy.org/phaser-tutorial-how-to-create-an-idle-clicker-game/

game.state.add('play', {
	preload: function(){
		game.load.image('alien:pink', 'assets/pink.png');
		game.load.image('alien:green', 'assets/green.png');
		game.load.image('alien:grey', 'assets/grey.png');
		game.load.image('alien:blue', 'assets/blue.png');

		game.load.image('bg:castle', 'assets/bg/castle.png');
		game.load.image('bg:desert', 'assets/bg/desert.png');
		game.load.image('bg:forest', 'assets/bg/forest.png');
		game.load.image('bg:talltrees', 'assets/bg/talltrees.png');

		alienData = [
			{name: 'Pink',  image: 'alien:pink',   maxHealth: 20 },
			{name: 'Green', image: 'alien:green',  maxHealth: 25 },
			{name: 'Grey',  image: 'alien:grey',   maxHealth: 10 },
			{name: 'Blue',  image: 'alien:blue',   maxHealth: 33 }
		];

		this.player = {
			clickDmg: 1,
			gold: 0
		};



	},
	create: function(){
		var state = this;
 
		this.background = this.game.add.group();
		var bg = state.game.add.tileSprite(0, 0, state.game.world.width,
            state.game.world.height, 'bg:castle', '', state.background);
		bg.tileScale.setTo(0.5, 0.5);

		// Alien Group
		this.aliens = this.game.add.group();
		var alien;
		alienData.forEach(function(data){
			alien = state.aliens.create(1000, state.game.world.centerY, data.image);
			alien.anchor.setTo(0.5);

			alien.details = data;

			alien.inputEnabled = true;
			alien.events.onInputDown.add(state.onClickAlien, state);

			alien.health = alien.maxHealth = data.maxHealth;

			alien.events.onKilled.add(state.onKilledAlien, state);
			alien.events.onRevived.add(state.onRevivedAlien, state);


		});
		// Current Alien
		this.currentAlien = this.aliens.getRandom();
		this.currentAlien.position.set(this.game.world.centerX + 100, this.game.world.centerY);

		// UI
		this.alienInfoUI = this.game.add.group();
		this.alienInfoUI.position.setTo(this.currentAlien.x - 220, this.currentAlien.y + 120);
		this.alienNameText = this.alienInfoUI.addChild(this.game.add.text(0, 0, this.currentAlien.details.name, {
			font: '48px Arial Black',
			fill: '#fff',
			strokeThickness: 4
		}));
		this.alienHealthText = this.alienInfoUI.addChild(this.game.add.text(0, 80, this.currentAlien.health + ' HP', {
			font: '32px Arial Black',
			fill: '#ff0000',
			strokeThickness: 4
		}));

		// DMG POOL
		this.dmgTextPool = this.add.group()
		var dmgText;
		for(var d=0;d<50;d++){
			dmgText = this.add.text(0,1, '1', {
				font: '64px Arial Black',
				fill: '#fff',
				strokeThickness: 4
			});
			dmgText.exists = false;
			dmgText.tween = game.add.tween(dmgText).to({
				alpha: 0,
				y: 100,
				x: this.game.rnd.integerInRange(100,700)
			}, 1000, Phaser.Easing.Cubic.Out);

			dmgText.tween.onComplete.add(function(text, tween){
				text.kill();
			});
			this.dmgTextPool.add(dmgText);
		}


	}, 
	render: function(){
		game.debug.text(
			this.currentAlien.details.name,
			this.game.world.centerX - this.currentAlien.width / 2,
			this.game.world.centerY - this.currentAlien.height / 2);

	},
	onClickAlien: function(alien, pointer){

		this.currentAlien.damage(this.player.clickDmg);
		this.alienHealthText.text = this.currentAlien.alive ? this.currentAlien.health + " HP" : "DEAD";

		var dmgText = this.dmgTextPool.getFirstExists(false);
		if (dmgText){
			dmgText.text = this.player.clickDmg;
			dmgText.reset(pointer.positionDown.x, pointer.positionDown.y);
			dmgText.alpha = 1;
			dmgText.tween.start();
		}

	},
	onKilledAlien: function(alien){
		alien.position.set(1000, this.game.world.centerY);
		this.currentAlien = this.aliens.getRandom();
		this.currentAlien.revive(this.currentAlien.maxHealth);
	},
	onRevivedAlien: function(alien){
		this.currentAlien.position.set(this.game.world.centerX + 100, this.game.world.centerY);
		this.alienNameText.text = alien.details.name;
		this.alienHealthText.text = alien.health + " HP";

	}



});

game.state.start('play');